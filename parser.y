/***********************************/
/*   Yacc specification for C--	   */
/*   compiler as per grammar in	   */
/*   Louden			   */
/***********************************/

%{
	#include <stdio.h>
	#include <string.h>
	#include "common.h"
	#include "lex.h"
	#include "AST.h"
	#include "memory.h"

	void yyerror(char *);
	static TokenType yylex();

	Token nextToken;

	TreeNode *savedTree;		//Node for completed tree
	
	int savedLineNumber = 0;
	int functionLineNumber = 0;
%}

%token	ELSE
%token	IF
%token	WHILE
%token	RETURN

%token	INT
%token	BOOLEAN
%token	CHARACTER
%token	VOID

%token	PLUS
%token	MINUS
%token	MULTIPLY
%token	DIVIDE
%token	LESS_THAN
%token	LESS_THAN_EQUAL_TO
%token	GREATER_THAN
%token	GREATER_THAN_EQUAL_TO
%token	EQUAL_TO
%token	NOT_EQUAL_TO
%token	ASSIGN
%token	SEMICOLON
%token	COMMA
%token	OPEN_PARENTHESIS
%token	CLOSE_PARENTHESIS
%token	OPEN_BRACKET
%token	CLOSE_BRACKET
%token	OPEN_BRACE
%token	CLOSE_BRACE
%token	OPEN_MULTILINE_COMMENT
%token	CLOSE_MULTILINE_COMMENT

%token	ID
%token	NUM
%token	QUOTECHAR
%token	END_OF_FILE
%token	ERROR

%union
{
	TreeNode *treeNode;
	int numVal;
	char *strVal;
	TypeSpec typeSpec;
	NodeType nodeType;
}

%type <treeNode> 	program declaration_list declaration var_declaration fun_declaration params param_list
%type <treeNode>	compound_statement param local_declarations statement_list statement
%type <treeNode>	expression_statement selection_statement iteration_statement return_statement
%type <treeNode>	expression simple_expression var additive_expression term factor call arg_list args
%type <numVal> 		numval
%type <strVal>		identifier
%type <typeSpec>	type_specifier
%type <nodeType>	relop addop mulop
%start program
%%

program	:	declaration_list
			{
				savedTree = $$;
				return $$;
			}
		;

declaration_list	:	declaration_list declaration	
					{
						TreeNode *t = $1;
						if (t != NULL)
						{
							while (t->sibling != NULL)
								t = t->sibling;
							t->sibling = $2;
							$$ = $1;
						}
						else
						{
							$$ = $2;
						}
					}
				| declaration
					{
						$$ = $1;
					}
				;

declaration		:	var_declaration	
					{
						$$ = $1;
					}
				| fun_declaration	
					{
						$$ = $1;
					}
				;


var_declaration		:	type_specifier identifier SEMICOLON
					{
						TreeNode *t = NEW(TreeNode);
						
						t->strVal = $2;
						t->nodeType = VARIABLE_DECLARATION_NODE;
						t->lineNumber = line_number;
						t->typeSpec = $1;

						$$ = t;
					}
				| type_specifier identifier OPEN_BRACKET numval CLOSE_BRACKET SEMICOLON	
					{
						TreeNode *t = NEW(TreeNode);

						t->strVal = $2;
						t->nodeType = ARRAY_DECLARATION_NODE;
						t->lineNumber = line_number;
						t->typeSpec = $1;
						t->numVal = $4;

						$$ = t;
					}
				;

numval			:	NUM
					{
						$$ = atoi(nextToken.tokenName);
					}

identifier		:	ID	
					{
						char *str = malloc(strlen(nextToken.tokenName));;
						strcpy(str, nextToken.tokenName);
						$$ = str;
					}

type_specifier		:	INT	
					{
						$$ = INT_TYPE;
					}
				| VOID
					{
						$$ = VOID_TYPE;
					}
				| CHARACTER
					{
						$$ = CHARACTER_TYPE;
					}
				| BOOLEAN	
					{
						$$ = BOOLEAN_TYPE;
					}
				;

fun_declaration		:	type_specifier identifier
					{
						functionLineNumber = line_number;
					}
				OPEN_PARENTHESIS params CLOSE_PARENTHESIS compound_statement
					{
						TreeNode *t = NEW(TreeNode);

						t->lineNumber = functionLineNumber;
						t->typeSpec = $1;
						t->strVal = $2;
						t->nodeType = DECLARATION_NODE;
						t->C1 = $5;	//Parameter list
						t->C2 = $7;	//Compound statement

						$$ = t;
					}
				;

params			:	param_list
					{
						$$ = $1;
					}
				| VOID
					{
						$$ = NULL;	//No parameters will mean empty child, right? CHECK
					}
				;

param_list		:	param_list COMMA param
					{
						TreeNode *t = $1;
						if (t != NULL)
						{
							while (t->sibling != NULL)
								t = t->sibling;
							t->sibling = $3;
							$$ = $1;
						}
						else
						{
							$$ = $3;
						}
					}
				| param
					{
						$$ = $1;
					}
				;

param			:	type_specifier identifier
					{
						TreeNode *t = NEW(TreeNode);

						t->lineNumber = line_number;
						t->typeSpec = $1;
						t->strVal = $2;
						t->nodeType = VARIABLE_DECLARATION_NODE;

						$$ = t;
					}
				| type_specifier identifier OPEN_BRACKET CLOSE_BRACKET
					{
						TreeNode *t = NEW(TreeNode);

						t->lineNumber = line_number;
						t->typeSpec = $1;
						t->strVal = $2;
						t->nodeType = ARRAY_DECLARATION_NODE;

						$$ = t;
					}
				;

compound_statement	:	OPEN_BRACE 
					{
						savedLineNumber = line_number;
					}
				local_declarations statement_list CLOSE_BRACE
					{
						TreeNode *t = NEW(TreeNode);

						t->lineNumber = savedLineNumber;
						t->nodeType = COMPOUND_STMT_NODE;
						t->C1 = $3;	//Local declarations
						t->C2 = $4;	//Statement list

						$$ = t;
					}
				;

local_declarations	:	/* empty */ 
					{
						$$ = NULL;		//Should it be NULL only? CHECK
					}
				|local_declarations var_declaration 
					{
						TreeNode *t = $1;
						if (t != NULL)
						{
							while (t->sibling != NULL)
								t = t->sibling;
							t->sibling = $2;
							$$ = $1;
						}
						else
						{
							$$ = $2;
						}

					}
				;

statement_list		:	/* empty */ 
					{
						$$ = NULL;	//CHECK
					}
				|statement_list statement
					{
						TreeNode *t = $1;
						if (t != NULL)
						{
							while (t->sibling != NULL)
								t = t->sibling;
							t->sibling = $2;
							$$ = $1;
						}
						else
						{
							$$ = $2;
						}

					}
				;

statement		:	expression_statement
					{
						$$ = $1;
					}
				| compound_statement
					{
						$$ = $1;
					}
				| selection_statement
					{
						$$ = $1;
					}
				| iteration_statement
					{
						$$ = $1;
					}
				| return_statement
					{
						$$ = $1;
					}
				;

expression_statement	:	expression SEMICOLON
					{
						$$ = $1;
					}
				| SEMICOLON
					{
						$$ = NULL;	//CHECK
					}
				;

selection_statement	:	IF OPEN_PARENTHESIS expression CLOSE_PARENTHESIS statement
					{
						TreeNode *t = NEW(TreeNode);

						t->lineNumber = line_number;
						t->nodeType = IF_NODE;
						t->C1 = $3;	//Expression
						t->C2 = $5;	//Statement

						$$ = t;
					}
				| IF OPEN_PARENTHESIS expression CLOSE_PARENTHESIS statement ELSE statement
					{
						TreeNode *t = NEW(TreeNode);

						t->lineNumber = line_number;
						t->nodeType = IF_NODE;
						t->C1 = $3;	//Expression
						t->C2 = $5;	//Statement
						t->C3 = $7;	//Else statement

						$$ = t;
					}
				;

iteration_statement	:	WHILE OPEN_PARENTHESIS expression CLOSE_PARENTHESIS statement
					{
						TreeNode *t = NEW(TreeNode);

						t->lineNumber = line_number;
						t->nodeType = WHILE_NODE;
						t->C1 = $3;	//Expression
						t->C2 = $5;	//Statement

						$$ = t;
					}
				;

return_statement	:	RETURN SEMICOLON
					{
						$$ = NULL;
					}
				| RETURN expression SEMICOLON
					{
						TreeNode *t = NEW(TreeNode);

						t->lineNumber = line_number;
						t->nodeType = RETURN_NODE;
						t->C1 = $2;

						$$ = t;
					}	
				;

expression		:	var ASSIGN expression
					{
						TreeNode *t = NEW(TreeNode);

						t->lineNumber = line_number;
						t->nodeType = ASSIGN_NODE;
						t->C1 = $1;
						t->C2 = $3;

						$$ = t;
					}
				| simple_expression
					{
						$$ = $1;
					}
				;

var			:	identifier
					{
						TreeNode *t = NEW(TreeNode);

						t->lineNumber = line_number;
						t->strVal = $1;
						t->nodeType = VARIABLE_NODE;

						$$ = t;
					}
				| identifier OPEN_BRACKET expression CLOSE_BRACKET
					{
						TreeNode *t = NEW(TreeNode);

						t->lineNumber = line_number;
						t->strVal = $1;
						t->nodeType = ARRAY_NODE;
						t->C1 = $3;

						$$ = t;
					}
				;

simple_expression	:	additive_expression relop additive_expression
					{
						TreeNode *t = NEW(TreeNode);
							
						t->lineNumber = line_number;
						t->C1 = $1;
						t->C2 = $3;
						t->nodeType = $2;

						$$ = t;
					}
				| additive_expression
					{
						$$ = $1;
					}
				;

relop			:	LESS_THAN_EQUAL_TO
					{
						$$ = LTE_NODE;
					}
				| LESS_THAN
					{
						$$ = LT_NODE;
					}
				| GREATER_THAN
					{
						$$ = GT_NODE;
					}
				| GREATER_THAN_EQUAL_TO
					{
						$$ = GTE_NODE;
					}
				| EQUAL_TO
					{
						$$ = EQ_NODE;
					}
				| NOT_EQUAL_TO
					{
						$$ = NEQ_NODE;
					}
				;

additive_expression	:	additive_expression addop term
					{
						TreeNode *t = NEW(TreeNode);

						t->lineNumber = line_number;
						t->C1 = $1;
						t->C2 = $3;

						t->nodeType = $2;

						$$ = t;
					}
				| term
					{
						$$ = $1;
					}
				;

addop			:	PLUS
					{
						$$ = ADD_NODE;
					}
				| MINUS
					{
						$$ = SUBTRACT_NODE;
					}	
				;

term			:	term mulop factor
					{
						TreeNode *t = NEW(TreeNode);

						t->lineNumber = line_number;
						t->C1 = $1;
						t->C2 = $3;
						t->nodeType = $2;

						$$ = t;
					}
				| factor
					{
						$$ = $1;
					}
				;

mulop			:	MULTIPLY
					{
						$$ = MULTIPLY_NODE;
					}
				| DIVIDE
					{
						$$ = DIVIDE_NODE;
					}
				;

factor			:	OPEN_PARENTHESIS expression CLOSE_PARENTHESIS 
					{
						$$ = $2;
					}
				| var
					{
						$$ = $1;
					}
				| call
					{
						$$ = $1;
					}
				| numval
					{
						TreeNode *t = NEW(TreeNode);
						t->lineNumber = line_number;
						t->nodeType = CONSTANT_NODE;
						t->numVal = $1;
						$$ = t;
					}
				;

call			:	identifier OPEN_PARENTHESIS args CLOSE_PARENTHESIS
					{
						TreeNode *t = NEW(TreeNode);

						t->lineNumber = line_number;
						t->nodeType = FNCALL_NODE;
						t->C1 = $3;
						t->strVal = $1;
						//How do we find the typespec?
						$$ = t;
					}
				;

args			:	/* empty */ 
					{
						$$ = NULL;
					}
				|arg_list
					{
						$$ = $1;
					}
				;

arg_list		:	arg_list COMMA expression
					{
						TreeNode *t = $1;
						if (t != NULL)
						{
							while (t->sibling != NULL)
								t = t->sibling;
							t->sibling = $3;
							$$ = $1;
						}
						else
						{
							$$ = $3;
						}
					}
				| expression
					{
						$$ = $1;
					}
				;


				
				
%%

static TokenType yylex()
{

	getToken(&nextToken);
#if DEBUG 
	fprintf(yyout, "%d %s", nextToken.tokenType, nextToken.tokenName);
#endif
	return nextToken.tokenType;
}

TreeNode *constructTree()
{
	TreeNode *t;
	t = yyparse();

//	printTree(savedTree);
//	printTree(t);

	return t;
}

void yyerror(char *errorMessage)
{
	fprintf(stderr, "Syntax error : Line number %d : Unexpected token \"%s\"\n", line_number, nextToken.tokenName);
}
