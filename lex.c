#include <string.h>
#include <stdio.h>
#include "common.h"

extern char *yytext;
extern TokenType yylex();

TokenType getToken(Token *nextToken)
{
	nextToken->tokenType = yylex();
	if(nextToken->tokenType == QUOTECHAR)
		strncpy(nextToken->tokenName, yytext + 1, 1);
	else
		strcpy(nextToken->tokenName, yytext);

	return nextToken->tokenType;	
}

void printToken(Token *token, FILE *of)
{
	switch(token->tokenType)
	{

	case ELSE:
	case IF:
	case WHILE:
	case RETURN:

	case INT:
	case BOOLEAN:
	case CHARACTER:
	case VOID:

	case PLUS:
	case MINUS:
	case MULTIPLY:
	case DIVIDE:
	case LESS_THAN:
	case LESS_THAN_EQUAL_TO:
	case GREATER_THAN:
	case GREATER_THAN_EQUAL_TO:
	case EQUAL_TO:
	case NOT_EQUAL_TO:
	case ASSIGN:
	case SEMICOLON:
	case COMMA:
	case OPEN_PARENTHESIS:
	case CLOSE_PARENTHESIS:
	case OPEN_BRACKET:
	case CLOSE_BRACKET:
	case OPEN_BRACE:
	case CLOSE_BRACE:
		fprintf(of, "%s\n", token->tokenName);
		break;
	case ID:
		fprintf(of, "ID: \"%s\"\n", token->tokenName);
		break;
	case NUM:
		fprintf(of, "NUM: \"%d\"\n", atoi(token->tokenName));
		break;
	case QUOTECHAR:
		fprintf(of, "QUOTECHAR: '%s'\n", token->tokenName);
		break;
	default:	//Should never reach here
		fprintf(of, "Unknown token \"%s\"\n", token->tokenName);
	}

}
