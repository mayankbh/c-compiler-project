CC=gcc
CFLAGS= -g -DYYTOKENTYPE
LDFLAGS=
HEADERS= lex.h parser.h common.h AST.h memory.h symtab.h hash.h codegenGeneric.h codegenSASM.h

LEXFILE= analyser.l
LEX= flex

PARSEFILE= parser.y
PARSE= bison

SOURCES= lex.c lex.yy.c parser.tab.c AST.c memory.c symtab.c hash.c testSymTab.c codegenGeneric.c codegenSASM.c
EXECUTABLE= testsymtab

%.tab.c: $(PARSEFILE) $(HEADERS)
	$(PARSE) $(PARSEFILE)

%.yy.c: $(LEXFILE) $(HEADERS)
	$(LEX) $(LEXFILE)



$(EXECUTABLE): $(SOURCES) $(HEADERS)
	$(CC) $(CFLAGS) $(SOURCES) -o $@ -lfl

backup: tar cvf cmm.tar *.[chly] Makefile *.cmm
