#ifndef AST
#define AST

#define TABSIZE 2 

typedef enum
{
	PROGRAM_NODE,
	VARIABLE_NODE,
	VARIABLE_DECLARATION_NODE,
	ARRAY_NODE,
	ARRAY_DECLARATION_NODE,
	FUNCTION_NODE,
	PARAM_LIST_NODE,
	COMPOUND_STMT_NODE,
	DECLARATION_NODE,
	IF_NODE,
	STATEMENT_NODE,
	WHILE_NODE,
	RETURN_NODE,
	FNCALL_NODE,
	ASSIGN_NODE,
	ADD_NODE,
	SUBTRACT_NODE,
	MULTIPLY_NODE,
	DIVIDE_NODE,
	LT_NODE,
	LTE_NODE,
	GT_NODE,
	GTE_NODE,
	NEQ_NODE,
	EQ_NODE,
	CONSTANT_NODE

} NodeType;

typedef enum
{
	NULL_TYPE,
	VOID_TYPE,
	INT_TYPE,
	CHARACTER_TYPE,
	BOOLEAN_TYPE
} TypeSpec;

typedef struct TreeNode
{	
	//Children
	struct TreeNode *C1;
	struct TreeNode *C2;
	struct TreeNode *C3;	

	//Sibling
	struct TreeNode *sibling;

	//Data
	int lineNumber;
	int numVal;
	char *strVal;
	NodeType nodeType;
	TypeSpec typeSpec;
	char *renStr;
} TreeNode;

void printNode(TreeNode *, int);
void printTree(TreeNode *);
void printNodeType(NodeType);
void printTypeSpec(TypeSpec);
void indent(int);
int isOperationNode(nodeType);

#endif
