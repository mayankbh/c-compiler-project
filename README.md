# README #

A compiler for C--, a reduced version of C. 

### What is this repository for? ###

* Currently prints symbol table and the abstract syntax tree

### How do I get set up? ###

* Run 'make'.
* Dependencies - flex, bison
* To execute - ./testsymtab <Filename> . If the file extension isn't specified, <Filename>.cmm is parsed by default.


### Who do I talk to? ###

* Mayank Bhatt - mayankbh@gmail.com
