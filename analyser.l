/*
	Lexical analyser for C-- compiler
	Syntax as given in Kenneth Louden - Compiler Construction
	Page 491-492

*/

%{
#include	<string.h>
#include	"common.h"

int line_number	= 1;
%}

DIGIT			[0-9]
LETTER			[a-zA-Z]
LETDIGIT		[0-9a-zA-Z]
ID			{LETTER}{LETDIGIT}*
NUM			{DIGIT}+
QUOTECHAR		'{LETDIGIT}'	

whitespace		[ \t]+
newline			\n

/* Have to support escape sequences in QUOTECHAR*/

%%


"else"		{ return ELSE;}
"if"		{ return IF;}
"while"		{ return WHILE;}
"return"	{ return RETURN;}
	
"int"		{ return INT;}
"boolean"	{ return BOOLEAN;}
"char"		{ return CHARACTER;}
"void"		{ return VOID;}

"/*"		{
			while(1)
			{
				char temp;
				while((temp = input()) != '*')	//Skip until '*' is encountered
					if(temp == '\n')
						line_number++;
				if((temp = input()) != '/')
				{
					if(temp == '\n')
						line_number++;
					unput(yytext[yyleng - 1]);	//Not a '/', put it back in stream
				}
				else			//End of comment
					break;
			}
		}	/*Process comments*/




"+"		{ return PLUS;}
"-"		{ return MINUS;}
"*"		{ return MULTIPLY;}
"/"		{ return DIVIDE;}
"<="		{ return LESS_THAN_EQUAL_TO;}
">="		{ return GREATER_THAN_EQUAL_TO;}
"<"		{ return LESS_THAN;}
">"		{ return GREATER_THAN;}
"=="		{ return EQUAL_TO;}
"!="		{ return NOT_EQUAL_TO;}
"="		{ return ASSIGN;}
";"		{ return SEMICOLON;}
","		{ return COMMA;}
"("		{ return OPEN_PARENTHESIS;}
")"		{ return CLOSE_PARENTHESIS;}
"["		{ return OPEN_BRACKET;}
"]"		{ return CLOSE_BRACKET;}
"{"		{ return OPEN_BRACE;}
"}"		{ return CLOSE_BRACE;}

{ID}		{ return ID;}
{NUM}		{ return NUM;}
{QUOTECHAR}	{ return QUOTECHAR;}

{whitespace}	{/*Skip whitespace */	}
{newline}	{ line_number++;	}		/*Increment line number*/

.		{ return ERROR;}
%%



