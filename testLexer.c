#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "common.h"
#include "lex.h"

extern FILE *yyin;

int main(int argc, char *argv[])
{
	FILE *in;
	FILE *out;

	char inFileName[40];
	char outFileName[40];

	Token nextToken;

	if(argc != 2)
	{
		printf("Usage: %s <filename>\n", argv[0]);
		exit(1);
	}	

	strcpy(inFileName, argv[1]);
	
	if(strchr(inFileName, '.') == NULL)
		strcat(inFileName, ".cmm");

	if((in = fopen(inFileName, "r")) == NULL)
	{
		printf("File %s not found\n", inFileName);
		exit(1);
	}	
	
	strcpy(outFileName, strtok(inFileName, "."));	
	strcat(outFileName, ".lst");

	if((out = fopen(outFileName, "w")) == NULL)
	{
		printf("Error creating file %s\n", outFileName);
		exit(1);
	}	

	yyin = in;

	do
	{
		getToken(&nextToken);
		if(nextToken.tokenType == ERROR)
		{
			printf("Unexpected token \"%s\"\n", nextToken.tokenName);
			break;
		}
		if(nextToken.tokenType != END_OF_FILE)
			printToken(&nextToken, out);
	}
	while(nextToken.tokenType != END_OF_FILE);

	//Remember to close files
}
