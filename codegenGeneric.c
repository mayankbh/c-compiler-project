#include <stdio.h>
#include "AST.h"
#include "codegenSASM.h"
#include "codegenGeneric.h"


void generateCode(TreeNode *treeRoot, FILE *outputFile, int targetArchitecture)
{
	TreeNode *ptr = treeRoot;

	while(ptr != NULL)
	{
		if(targetArchitecture == SASM)
		{
			generateSASMCode(&ptr, outputFile);
		}
	}
}
