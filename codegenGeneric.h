#ifndef CODEGENGENERIC
#define CODEGENGENERIC

#define SASM 1

void generateCode(TreeNode *treeRoot, FILE *outputFile, int targetArchitecture);

#endif
