#ifndef MEMORY
#define MEMORY

#define NEW(type) (type *) Malloc(sizeof(type))

void *Malloc(int);		//Attempts to allocate memory designated by (size). Also initializes entire block to 0. On success, it returns a pointer to the allocated memory. On failure, it prints an error message to STDERR and exits.

#endif
