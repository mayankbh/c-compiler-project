#ifndef CODEGENSASM
#define CODEGENSASM

void generateSASMVariable(TreeNode *node, FILE *outputFile);
void generateSASMArray(TreeNode *node, FILE *outputFile);
void generateSASMDataSegment(TreeNode **ptr, FILE *outputFile);
void generateSASMCode(TreeNode **ptr, FILE *outputFile);

#endif
