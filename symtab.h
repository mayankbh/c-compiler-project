#ifndef SYMTAB
#define SYMTAB

#define SYMBOLTABLESIZE 	500		//Remember to define this
#define HASH(name) hash_djb2(name)%SYMBOLTABLESIZE
#define MAXSYMBOLLENGTH 80

typedef enum
{
	VARIABLE_S,
	ARRAY_S,
	FUNCTION_S
} SymbolType;

typedef struct Symbol
{
	char name[MAXSYMBOLLENGTH];
	char renStr[MAXSYMBOLLENGTH];
	SymbolType symbolType;
	TypeSpec typeSpec;
	int value;
	struct Symbol *next;
} Symbol;

typedef struct SymbolTable
{
	Symbol *symbols [SYMBOLTABLESIZE];		//Hash table for symbols
	struct SymbolTable *parent;			//Pointer to parent symbol table
	int scope;					//Tracks indentation
} SymbolTable;

typedef struct SymbolTableListNode
{
	int printed;
	SymbolTable *table;				//Symbol table
	struct SymbolTableListNode *next;		//Pointer to next node
} SymbolTableListNode;

SymbolTable *createSymbolTable();	//Allocates memory for a new symbol table and returns a pointer if successful. Exits on failure

Symbol *putSymbol (SymbolTable *, char *, char *, SymbolType, TypeSpec);	//Adds entry for symbol in current scope and returns a pointer to entry on success, and NULL on failure

Symbol *getSymbol (SymbolTable *, char *);	//Searches scope chain for symbol, and returns a pointer to the symbol if found, NULL if not found

int defSymbol (SymbolTable *, char *);	//Checks if symbol is already defined in present scope, returns 1 if found, otherwise 0

void addLinkedList(SymbolTable *t, TreeNode *l);	//Accepts a linked list of TreeNodes and attempts to add it to the symbol table

void processAST(SymbolTable *, TreeNode *);	//Accepts an AST and creates a symbol table from it

void appendSymbolTableList(SymbolTable *);	//Appends given symbol table to global list

void printSymbol(Symbol *);

void printSymbolTable(SymbolTable *);	//Prints the symbol table with specified indentation

void printSymbolTableList();	//Prints all the symbol tables in the list

void generateRenameString(char *);

void printSymbolType(SymbolType);


#endif
