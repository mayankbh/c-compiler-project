#include <stdlib.h>
#include <stdio.h>
#include "memory.h"

void *Malloc(int size)
{
	void *ptr;
	int i = 0;

	ptr = malloc(size);

	if(ptr == NULL)
	{
		fprintf(stderr, "Problem occurred during memory allocation. Exiting.\n");
		exit(1);
	}
	//Success

	for(i = 0; i < size; i++)
		*((unsigned char *) (ptr + i)) = 0;		//Initialize block to 0

	return ptr;
}
