#include <stdio.h>
#include "AST.h"
#include "codegenSASM.h"

void generateSASMVariable(TreeNode *node, FILE *outputFile)
{
	switch(node->typeSpec)
	{
		case INT_TYPE:
			fprintf(outputFile, "%s\tdw\t%d\n", node->strVal, 0);	//Declare as word, set as 0
			break;

		case CHARACTER_TYPE:
			fprintf(outputFile, "%s\tdb\t%d\n", node->strVal, 0);
			break;

		case BOOLEAN_TYPE:
			fprintf(outputFile, "%s\tdb\t%d\n", node->strVal, 0);
			break;
	}
}

void generateSASMArray(TreeNode *node, FILE *outputFile)
{
	switch(node->typeSpec)
	{
		case INT_TYPE:
			fprintf(outputFile, "%s\trs\t%d\n", node->strVal, node->numVal * 2);	//Declare as word, set as 0
			break;

		case CHARACTER_TYPE:
			fprintf(outputFile, "%s\trs\t%d\n", node->strVal, node->numVal);
			break;

		case BOOLEAN_TYPE:
			fprintf(outputFile, "%s\trs\t%d\n", node->strVal, node->numVal);
			break;
	}
}

void generateSASMDataSegment(TreeNode **ptr, FILE *outputFile)
{
	fprintf(outputFile, ".data\n");

	while((*ptr)->nodeType == VARIABLE_DECLARATION_NODE || (*ptr)->nodeType == ARRAY_DECLARATION_NODE)
	{
		if((*ptr)->nodeType == VARIABLE_DECLARATION_NODE)
			generateSASMVariable(*ptr, outputFile);
		else
			generateSASMArray(*ptr, outputFile);
		
		//Move to next node

		*ptr = (*ptr)->sibling;
		if(*ptr == NULL)
			break;
	}

	fprintf(outputFile, ".end\n");
}

void generateSASMCode(TreeNode **ptr, FILE *outputFile)
{
	if((*ptr)->nodeType == VARIABLE_DECLARATION_NODE || (*ptr)->nodeType == ARRAY_DECLARATION_NODE)
	{
		generateSASMDataSegment(ptr, outputFile);
	}
}
