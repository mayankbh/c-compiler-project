#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "common.h"
#include "lex.h"
#include "parser.h"
#include "AST.h"
#include "symtab.h"
#include "memory.h"
#include "codegenGeneric.h"

extern FILE *yyin;
extern FILE *yyout;

extern int yydebug;
extern SymbolTableListNode *symbolTableList;


int main(int argc, char *argv[])
{
#ifdef YYDEBUG
	yydebug = 1;		//Debugging purposes
#endif
	FILE *in;
	FILE *out;
	FILE *targetFile;

	TreeNode *tree;
	SymbolTable *root;

	root = NEW(SymbolTable);
	symbolTableList = NEW(SymbolTableListNode);

	symbolTableList->table = root;

	char inFileName[40];
	char outFileName[40];
	char asmFileName[40];
	if(argc != 2)
	{
		printf("Usage: %s <filename>\n", argv[0]);
		exit(1);
	}	

	strcpy(inFileName, argv[1]);
	
	if(strchr(inFileName, '.') == NULL)
		strcat(inFileName, ".cmm");

	if((in = fopen(inFileName, "r")) == NULL)
	{
		printf("File %s not found\n", inFileName);
		exit(1);
	}	
	
	strcpy(outFileName, strtok(inFileName, "."));	
	strcat(outFileName, ".lst");

	if((out = fopen(outFileName, "w")) == NULL)
	{
		printf("Error creating file %s\n", outFileName);
		exit(1);
	}	

	yyin = in;
	yyout = out;

	tree = yyparse();
	
	processAST(root, tree);
	printSymbolTableList();
	printTree(tree);

	/*
	strcpy(asmFileName, strtok(inFileName, "."));	
	strcat(asmFileName, ".asm");
	
	
	if((targetFile = fopen(asmFileName, "w")) == NULL)
	{
		printf("Error creating file %s\n", asmFileName);
		exit(1);
	}	

	generateCode(tree, targetFile, SASM);	
	*/
	//Remember to close files
	
//	fclose(targetFile);
	fclose(in);
	fclose(out);
	//Remember to close files
}
