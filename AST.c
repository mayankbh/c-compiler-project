#include <stdlib.h>
#include <stdio.h>
#include "AST.h"

TreeNode *newNode()
{
	//Declare pointer
	TreeNode *ptr;
	//Allocate memory
	ptr = (TreeNode *) malloc(sizeof(TreeNode));

	//Initialize child/sibling nodes to NULL
	ptr->C1 = NULL;
	ptr->C2 = NULL;
	ptr->C3 = NULL;
	ptr->sibling = NULL;
	//Initialize other values to 0/NULL
	ptr->lineNumber = 0;
	ptr->numVal = 0;
	ptr->nodeType = 0;
	ptr->typeSpec = 0;
	ptr->strVal = NULL;
	ptr->renStr = NULL;

	//Return pointer to this new node
	return ptr;
}

void indent(int indentation)
{
	int j;
	for(j = 0; j < indentation * TABSIZE; j++)
		printf(" ");
}

void printTypeSpec(TypeSpec type)
{
	printf("Data type : ");
	switch(type)
	{
		case NULL_TYPE:
			printf("(Null)\n");
			break;
		case INT_TYPE:
			printf("Int\n");
			break;
		case VOID_TYPE:
			printf("Void\n");
			break;
		case CHARACTER_TYPE:
			printf("Char\n");
			break;
		case BOOLEAN_TYPE:
			printf("Boolean\n");
			break;
	}
}

void printNodeType(NodeType type)
{
	switch(type)
	{
		case PROGRAM_NODE:
			printf("Program \n");
			break;
		case VARIABLE_NODE:
			printf("Variable \n");
			break;
		case VARIABLE_DECLARATION_NODE:
			printf("Variable Declaration\n");
			break;
		case ARRAY_NODE:
		 	printf("Array \n");
			break;
		case ARRAY_DECLARATION_NODE:
		 	printf("Array Declaration\n");
			break;
		case FUNCTION_NODE:
			printf("Function \n");
			break;
		case PARAM_LIST_NODE:
			printf("Parameter List \n");
			break;
		case COMPOUND_STMT_NODE:
			printf("Compound Statement \n");
			break;
		case DECLARATION_NODE:
			printf("Declaration \n");
			break;
		case IF_NODE:
			printf("If \n");
			break;
		case STATEMENT_NODE:
			printf("Statement \n");
			break;
		case WHILE_NODE:
			printf("While \n");
			break;
		case RETURN_NODE:
			printf("Return \n");
			break;
		case FNCALL_NODE:
			printf("Function Call \n");
			break;
		case CONSTANT_NODE:
			printf("Constant\n");
			break;
		case ASSIGN_NODE:
			printf("=\n");
			break;
		case ADD_NODE:
			printf("+\n");
			break;
		case SUBTRACT_NODE:
			printf("- \n");
			break;
		case MULTIPLY_NODE:
			printf("* \n");
			break;
		case DIVIDE_NODE:
			printf("/ \n");
			break;
		case LT_NODE:
			printf("< \n");
			break;
		case LTE_NODE:
			printf("<= \n");
			break;
		case GT_NODE:
			printf("> \n");
			break;
		case GTE_NODE:
			printf(">= \n");
			break;
		case NEQ_NODE:
			printf("!= \n");
			break;
		case EQ_NODE:
			printf("==  \n");
			break;
		
	}
}


void printNode(TreeNode *t, int indentation)
{
	indent(indentation);
	printf("Line number : %d\n", t->lineNumber);

	indent(indentation);
	printf("Numeric value : %d\n", t->numVal);

	indent(indentation);
	printf("String value : %s\n", t->strVal);

	indent(indentation);
	printf("Node type : ");
	printNodeType(t->nodeType);

	indent(indentation);
	printTypeSpec(t->typeSpec);

	indent(indentation);
	printf("Temporary name : %s\n", t->renStr);

	if(t->C1 != NULL)
	{
		printf("\n");
		indent(indentation + 1);
		printf("C1\n");
		printNode(t->C1, indentation + 1);
	}
	if(t->C2 != NULL)
	{
		printf("\n");
		indent(indentation + 1);
		printf("C2\n");
		printNode(t->C2, indentation + 1);
	}
	if(t->C3 != NULL)
	{
		printf("\n");
		indent(indentation + 1);
		printf("C3\n");
		printNode(t->C3, indentation + 1);
	}
	if(t->sibling != NULL)
	{
		printf("\n");
		printNode(t->sibling, indentation);
	}
}	


void printTree(TreeNode *tree)
{
	printNode(tree, 0);
}

int isOperationNode(NodeType type)
{
	switch(type)
	{
		case	FNCALL_NODE:
		case	ASSIGN_NODE:
		case	ADD_NODE:
		case	SUBTRACT_NODE:
		case	MULTIPLY_NODE:
		case	DIVIDE_NODE:
		case	LT_NODE:
		case	LTE_NODE:
		case	GT_NODE:
		case	GTE_NODE:
		case	NEQ_NODE:
		case	EQ_NODE:
			return 1;

		default:
			return 0;
	}
}
