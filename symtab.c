#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "memory.h"
#include "AST.h"
#include "symtab.h"

int renameStringIndex = 0;

SymbolTableListNode *symbolTableList = NULL;		//Linked list to track the symbol tables

SymbolTable *createSymbolTable()
{
	SymbolTable *ptr = NEW(SymbolTable);		//Allocate memory
	return ptr;
}

Symbol *putSymbol(SymbolTable *t, char *name, char *renStr, SymbolType type, TypeSpec typeSpec)
{
	unsigned long hash = HASH(name);
	Symbol *ptr;
	Symbol *temp;

	if(t->symbols[hash] == NULL)	//Symbol doesn't exist, no hash collision
	{
		t->symbols[hash] = NEW(Symbol);		//Create an entry
		strcpy(t->symbols[hash]->name, name);	//Copy over the name
		strcpy(t->symbols[hash]->renStr, renStr);	//Copy temporary name
		t->symbols[hash]->symbolType = type;	//Copy type (Function/variable/array)
		t->symbols[hash]->typeSpec = typeSpec;	//Copy type specifier	(int, char etc)
		return t->symbols[hash];
	}

	//Hash collision occurred, traverse linked list to check if symbol already exists

	ptr = t->symbols[hash];		//Set to first entry
	while(ptr != NULL)
	{
		if(strcmp(ptr->name, name) == 0)	//Symbol with same name in same scope
		{
			fprintf(stderr, "Duplicate definition of symbol : %s\n", name);
			return NULL;			//Return a NULL pointer to indicate failure
		}
		ptr = ptr->next;			//Move to next entry
	}
	
	//Name not found, insert into hash bucket

	ptr = NEW(Symbol);		//Create a new entry
	strcpy(ptr->name, name);	//Copy over the name
	strcpy(t->symbols[hash]->renStr, renStr);	//Copy temporary name
	ptr->symbolType = type;	//Copy type
	t->symbols[hash]->typeSpec = typeSpec;	//Copy type specifier	(int, char etc)

	temp = t->symbols[hash];
	t->symbols[hash] = ptr;		//Set new node at first position
	ptr->next = temp;		//Append the rest of the list to this node

	return ptr;			//Return a pointer to the newly created node
}

Symbol *getSymbol(SymbolTable *t, char *name)
{
	unsigned long hash = HASH(name);
	Symbol *ptr;

	if(defSymbol(t, name))		//Check if symbol is defined in present scope
	{
		ptr = t->symbols[hash];	//Set to first entry
		while(strcmp(ptr->name, name) != 0);	//Until the entry is found
		//ptr now points to appropriate entry
		return ptr;		//Return a pointer to the symbol
	}

	//Not found in hash bucket, check if the parent exists
	if(t->parent != NULL)
		return getSymbol(t->parent, name);	//Search the parent symbol table
	
	//No parent exists, hence symbol doesn't exist
	return NULL;
}

int defSymbol(SymbolTable *t, char *name)
{
	unsigned long hash = HASH(name);
	Symbol *ptr;

	if(t->symbols[hash] == NULL)	//Hash does not exist, symbol cannot be present
		return 0;		//Symbol not found

	//Hash found, check corresponding hash bucket for entry
	ptr = t->symbols[hash];	//
	while(ptr != NULL)
	{
		if(strcmp(ptr->name, name) == 0)	//Symbol found
			return 1;			//Return a pointer to the symbol
		ptr = ptr->next;			//Move to next entry
	}

	//Not found in hash bucket, return 0
	return 0;

}

void addLinkedList(SymbolTable *t, TreeNode *l)
{
	TreeNode *ptr;
	Symbol *temp;
	char renStr[MAXSYMBOLLENGTH];
	ptr = l;

	while(ptr != NULL)
	{
		if(ptr->nodeType == VARIABLE_DECLARATION_NODE)
		{
			generateRenameString(renStr);
			ptr->renStr = malloc(strlen(renStr));
			strcpy(ptr->renStr, renStr);
			temp = putSymbol(t, ptr->strVal, renStr, VARIABLE_S, ptr->typeSpec);	//Add to current scope
		}
		if(ptr->nodeType == ARRAY_DECLARATION_NODE)
		{
			generateRenameString(renStr);
			ptr->renStr = malloc(strlen(renStr));
			strcpy(ptr->renStr, renStr);
			temp = putSymbol(t, ptr->strVal, renStr, ARRAY_S, ptr->typeSpec);	//Add to current scope
		}
		if(temp == NULL)
			return;
		ptr = ptr->sibling;
	}
}


void processAST(SymbolTable *table, TreeNode *tree)
{
	SymbolTable *temp;
	SymbolTable *temp2;
	Symbol *var;
	char renStr[MAXSYMBOLLENGTH] = "";

	if(tree == NULL)	//Empty node, no processing needed
		return;

	if(tree->nodeType == VARIABLE_DECLARATION_NODE)
	{
		generateRenameString(renStr);			//Generate a new rename string
		tree->renStr = malloc(strlen(renStr));
		strcpy(tree->renStr, renStr);			//Copy new rename string into AST node
		putSymbol(table, tree->strVal, renStr, VARIABLE_S, tree->typeSpec);	//Add to current scope
	}

	if(tree->nodeType == ARRAY_DECLARATION_NODE)
	{
		generateRenameString(renStr);			//Generate a new rename string
		tree->renStr = malloc(strlen(renStr));
		strcpy(tree->renStr, renStr);			//Copy new rename string into AST node
		putSymbol(table, tree->strVal, renStr, ARRAY_S, tree->typeSpec);	//Add to current scope
	}

	if(tree->nodeType == DECLARATION_NODE)
	{
		generateRenameString(renStr);			//Generate a new rename string
		tree->renStr = malloc(strlen(renStr));
		strcpy(tree->renStr, renStr);			//Copy new rename string into AST node
		putSymbol(table, tree->strVal, renStr, FUNCTION_S, tree->typeSpec);	//Add function to current scope, process children
		temp = NEW(SymbolTable);		//Create a new symbol table for new scope
		temp->scope = table->scope + 1;		//Child gets scope = (parent + 1)
		appendSymbolTableList(temp);		//Add new symbol table to linked list
		temp->parent = table;			//Set current table as parent of newly created table
		addLinkedList(temp, tree->C1);		//Add parameters of function to the new scope
		addLinkedList(temp, tree->C2->C1);	//Process the declarations in the compound statement
		processAST(temp, tree->C2->C2);		//Process the statement list in the compound statement
	}

	if(tree->nodeType == COMPOUND_STMT_NODE)
	{
		temp = NEW(SymbolTable);
		temp->scope = table->scope + 1;		//Child gets scope = (parent + 1)
		appendSymbolTableList(temp);		//Add new symbol table to linked list
		temp->parent = table;			//Set current table as parent of newly created table
		addLinkedList(temp, tree->C1);		//Add the declarations to the new scope
		processAST(temp, tree->C2);		//Process the statement list under new scope
	}

	if(tree->nodeType == IF_NODE)
	{
		temp = NEW(SymbolTable);
		temp->scope = table->scope + 1;		//Child gets scope = (parent + 1)
		appendSymbolTableList(temp);		//Add new symbol table to linked list
		temp->parent = table;			//Set current table as parent of newly created table
		temp2 = NEW(SymbolTable);
		temp2->scope = table->scope + 1;		//Child gets scope = (parent + 1)
		appendSymbolTableList(temp2);		//Add new symbol table to linked list
		temp2->parent = table;			//Set current table as parent of newly created table

		addLinkedList(temp, tree->C1->C1);
		addLinkedList(temp2, tree->C2->C1);	//Process declaration lists under separated scopes
		processAST(temp, tree->C1->C2);		//Process the two statement lists under separate scopes
		processAST(temp2, tree->C2->C2);
	}

	if(tree->nodeType == WHILE_NODE)
	{
		temp = NEW(SymbolTable);
		temp->scope = table->scope + 1;		//Child gets scope = (parent + 1)
		appendSymbolTableList(temp);		//Add new symbol table to linked list
		temp->parent = table;			//Set current table as parent of newly created table
		
		addLinkedList(temp, tree->C2->C1);	//Process declaration list
		processAST(temp, tree->C2->C2);		//Process statement list
	}

	if(isOperationNode(tree->nodeType))	//Check if the node is an operation (function call, addition etc)
	{
		if(tree->nodeType == FNCALL_NODE)
		{
			var = getSymbol(table, tree->strVal);
			if(var == NULL)
				fprintf(stderr, "Undefined function : %s\n", tree->strVal);
				
		}
		processAST(table, tree->C1);
		processAST(table, tree->C2);		//Process both children to check for undeclared variables
	}


	if(tree->nodeType == VARIABLE_NODE)
	{
		var = getSymbol(table, tree->strVal);
		if(var == NULL)		//Symbol doesn't exist
			fprintf(stderr, "Undefined symbol : %s\n", tree->strVal);
		else			//Symbol found
		{
			tree->renStr = malloc(strlen(var->renStr));
			strcpy(tree->renStr, var->renStr);		//Copy over rename string from table into AST
		}
	}

	if(tree->nodeType == ARRAY_NODE)
	{
		var = getSymbol(table, tree->strVal);
		if(var == NULL)		//Symbol doesn't exist
			fprintf(stderr, "Undefined symbol : %s\n", tree->strVal);
		else			//Symbol found
		{
			tree->renStr = malloc(strlen(var->renStr));
			strcpy(tree->renStr, var->renStr);		//Copy over rename string from table into AST
		}

		processAST(table, tree->C1);		//Index may be a variable, process it
	}
	//Current node and its children processed, proceed to process the sibling node

	processAST(table, tree->sibling);
}

void appendSymbolTableList(SymbolTable *t)
{
	SymbolTableListNode *ptr;
	SymbolTableListNode *temp;

	ptr = NEW(SymbolTableListNode);
	ptr->table = t;

	temp = symbolTableList;

	while(temp->next != NULL)
	{
		temp = temp->next;
	}
	temp->next = ptr;
}


void printSymbol(Symbol *s)
{
	printf("%s : ", s->name);
	printf("%s : ", s->renStr);
	printSymbolType(s->symbolType);
	printf(" : ");
	printTypeSpec(s->typeSpec);
}

void printSymbolTable(SymbolTable *t)
{
	Symbol *ptr;
	int i;

	for(i = 0; i < SYMBOLTABLESIZE; i++)
	{
		ptr = t->symbols[i];
		while(ptr != NULL)
		{
			indent(t->scope);
			printSymbol(ptr);
			ptr = ptr->next;
		}
	}
	indent(t->scope);
	printf("-----------------------\n");
}

void printSymbolTableList()
{
	SymbolTableListNode *ptr;
	SymbolTableListNode *temp;
	ptr = symbolTableList;

	while(ptr != NULL)
	{
		printSymbolTable(ptr->table);
		ptr = ptr->next;
	}
}

void generateRenameString(char *renStr)
{
	sprintf(renStr, "tmp%04d", renameStringIndex);
	renameStringIndex++;
}

void printSymbolType(SymbolType type)
{
	switch(type)
	{
		case VARIABLE_S:
			printf("Variable ");
			break;
		case ARRAY_S:
			printf("Array  ");
			break;
		case FUNCTION_S:
			printf("Function ");
			break;
	}
}
